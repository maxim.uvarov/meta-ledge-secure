FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

COMPATIBLE_MACHINE = "(ledge-qemuarm64|ledge-qemuarm)"

# 3.16
PV="3.16.0+git${SRCPV}"
SRCREV_ledgecommon = "06db73b3f3fdb8d23eceaedbc46c49c0b45fd1e2"

DEPENDS:append_ledgecommon += "python3-pycryptodomex-native python3-pycrypto-native"

SRC_URI:append = " \
file://0001-libckteec-add-support-for-ECDH-derive.patch \
	file://0002-tee-supplicant-introduce-struct-tee_supplicant_param.patch \
	file://0003-tee-supplicant-refactor-argument-parsing-in-main.patch \
	file://0004-tee-supplicant-rpmb-introduce-readn-wrapper-to-the-r.patch \
	file://0005-tee-supplicant-rpmb-read-CID-in-one-go.patch \
	file://0006-tee-supplicant-add-rpmb-cid-command-line-option.patch \
	"

EXTRA_OECMAKE:append = " \
    -DRPMB_EMU=0 \
"


